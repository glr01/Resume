import React from "react"
import styles from "./styles.module.scss"

export function SkillBars({ skills }) {
  return (
    <div className={styles.skillBars}>
      {skills.map((skill, index) => (
        <div key={index} className={styles.bar}>
          <div className={styles.info}>
            <p>{skill}</p>
          </div>
          <div className={styles.progressLine}>
            <div
              className={`${styles[skill.replace(" ", "")]} ${styles.skill}`}
            ></div>
          </div>
        </div>
      ))}
    </div>
  )
}
