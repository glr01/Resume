import React from "react"
import { SkillBars } from "../SkillBars"

export function Languages() {
  const languages = ["Inglês", "Português"]

  return <SkillBars skills={languages} />
}
