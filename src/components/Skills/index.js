import React from "react"
import { SkillBars } from "../SkillBars"

export function Skills() {
  const skills = [
    "HTML",
    "CSS",
    "React",
    "Nodejs",
    "JavaScript",
    "React Native",
    "MongoDB",
    "Prisma",
    "TypeScript"
  ]

  return <SkillBars skills={skills} />
}
