import React from "react"
import styles from "./styles.module.scss"

export function Header() {
  return (
    <div className={styles.headerContainer}>
      <h1>
        GABRIEL
        <br />
        RASTELI
      </h1>
      <div className={styles.myInfo}>
        <span className={styles.profession}>Desenvolder Full-Stack</span>
        <span className={styles.email}>gabrielrasteli3@gmail.com</span>
        <span className={styles.phone}>+55 16 999664381</span>
      </div>
    </div>
  )
}
