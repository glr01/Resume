import React from "react"
import { Skills } from "../Skills"
import { Languages } from "../Languages"
import styles from "./styles.module.scss"

export function Body() {
  const links = [
    {
      ref: "https://rasteli.vercel.app",
      label: "Portfólio (rasteli.vercel.app)"
    },
    {
      ref: "https://www.linkedin.com/mwlite/in/gabriel-rasteli-aab3451a0",
      label: "LinkedIn (Gabriel Rasteli)"
    },
    {
      ref: "https://github.com/rasteli",
      label: "GitHub (github.com/rasteli)"
    }
  ]

  const courses = [
    {
      name: "Curso de idiomas: inglês, CCAA - Orlândia",
      period: "fevereiro 2011 – dezembro 2019"
    },
    {
      name: "Curso em Vídeo - Javascript, Curso em Vídeo",
      period: "junho 2018 – julho 2018"
    },
    {
      name: "Curso em Vídeo - HTML5 e CSS3, Curso em Vídeo",
      period: "junho 2018 – julho 2018"
    },
    {
      name: "Curso em Vídeo - Algoritmos, Curso em Vídeo",
      period: "julho 2019 – agosto 2019"
    }
  ]

  return (
    <div className={styles.bodyContainer}>
      <div className={styles.profile}>
        <h4 className={styles.headings}>PERFIL</h4>
        <p>
          Desenvolvedor web, 19 anos, full-stack com mais de 3 anos de
          experiência, primariamente focado em desenvolvimento front-end.
        </p>

        <span>Stack</span>
        <ul>
          <li>
            Node.js <em>(back)</em>
          </li>
          <li>
            ReactJS <em>(front-web)</em>
          </li>
          <li>
            React Native <em>(front-mobile)</em>
          </li>
        </ul>
      </div>

      <div className={styles.links}>
        <h4 className={styles.headings}>LINKS</h4>
        {links.map((link, index) => (
          <a key={index} href={link.ref}>
            {link.label}
          </a>
        ))}
      </div>

      <div className={styles.skills}>
        <h4 className={styles.headings}>COMPETÊNCIAS</h4>
        <Skills />
      </div>
      <div className={styles.skills}>
        <h4 className={styles.headings}>IDIOMAS</h4>
        <Languages />
      </div>

      <div className={styles.courses}>
        <h4 className={styles.headings}>CURSOS</h4>
        {courses.map((course, index) => (
          <div key={index} className={styles.course}>
            <span className={styles.courseName}>{course.name}</span>
            <span className={styles.period}>{course.period}</span>
          </div>
        ))}
      </div>
    </div>
  )
}
